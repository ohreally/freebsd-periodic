# FreeBSD periodic

Periodic Tor status report on FreeBSD (see [periodic(8)](https://www.freebsd.org/cgi/man.cgi?query=periodic&apropos=0&sektion=0&manpath=FreeBSD+13.0-RELEASE+and+Ports&arch=default&format=html)).
